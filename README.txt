wp_admin

A tidy little module to help ease the transition from WordPress administration
* adds a path alias from "/wp-admin" to "/admin"
